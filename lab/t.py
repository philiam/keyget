#!/bin/env python
# coding: utf-8
# author: ZhangTao
# Date  : 2017/8/31
# from __future__ import unicode_literals

import zlib

"""
字符串加密
"""

import base64

url = 'http://kg.iot-c.top:9012/api/access'


def encrypt(url, pwd=''):
    b = zlib.compress(pwd + url + pwd)
    return base64.b64encode(b)

print (encrypt(url, 'www.qq.com'))


def gen_zip(name):
    f = open(name + '.pub')
    con = f.read()
    f.close()

    pz = zlib.compress(con)

    out = '../syskey/' + name + '.pz'
    f2 = open(out, mode='wb')
    f2.write(pz)
    f2.close()
    print(out + ' generated !')

    # read test
    f3 = open(out, mode='r')
    c = f3.read()
    f3.close()
    print('read out: \n' + zlib.decompress(c))

# gen_zip('keyget-v2')

