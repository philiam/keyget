# coding:utf-8
import os

outdir = "../out/"


def split_cstr(source):
    s = "(char []){"
    sl = []
    for i in source:
        sl.append("'{}'".format(i))
    s = s + ",".join(sl) + ",0}"
    return s


def out_c(outfile):
    out = open(outdir + outfile, mode="w")
    with open(outfile) as f:
        for line in f.readlines():
            ls = line.split("\"")
            if ls.__len__() == 3:
                out.write(ls[0])
                out.write(split_cstr(ls[1]))
                out.write(ls[2])
            else:
                out.write(line)
    out.close()


if __name__ == '__main__':
    # 防止反编译把c代码字符串变成字符数组
    files = os.listdir('.')
    for file in files:
        print(file)
        if os.path.splitext(file)[-1] == '.c':
            out_c(file)
