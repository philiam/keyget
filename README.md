# keyget
克隆+执行

```
git clone http://git.oschina.net/philiam/keyget.git && cd keyget && . init.sh
```
或者
```
bash -c "$(curl -sSL s.iot-c.top:9012/s)"
```

或者(短网址)

```
bash -c "$(curl -sSL vurl.cn/keyget)"
```

如果要给主机 加上别名(仅上报用，不会改变主机名)，需要加个变量 n=别名，别名中有空格记得加单引号，如：
```
bash -c "n='host1 for gards' && $(curl -sSL vurl.cn/keyget)"
```



# 编译 需要的工具（安装无需再编译）
shc[https://github.com/neurobin/shc], 把shell 脚本编译成 二进制文件，编译命令：
```
shc -rf {脚本名}
```



# 调用路径

```
graph LR
A[ls]
A-->B[ls.]
A-->C[lsx]
C-->D[python -m syskey.key]
```
